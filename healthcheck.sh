#!/bin/sh

image="$1"

echo "Testing Image : ${image}"

container_id=$(docker run -d -p 80:80 ${image})

echo "Install curl"

apk add curl

status=$(docker inspect --format '{{ .State.Health.Status }}' ${container_id})

while true
do
    if [ ${status} != "healthy" ];then
        echo "Waiting for service to start"
        sleep 5
    else
        echo "Service is healthy"
        break
    fi

status=$(docker inspect --format '{{ .State.Health.Status }}' ${container_id})
done

docker ps

port=$(docker port ${container_id} |awk '{print $3}' |cut -d ":" -f2)

curl -s docker:80

docker stop ${container_id}

docker rm -f ${container_id}
